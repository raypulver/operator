# operator

Converting block statements into chainable function expressions for use with expression arrow functions or otherwise.

## if/when

Function that accepts a conditional expression and returns a function that accepts the conditional body as a function and returns a callable object that can either be called to execute the conditional expression or can be chained with calls to `elseif`, which operates the same way as `if`, or can be chained with a call to `else` which accepts a function body that is executed if none of the previous conditional expressions were truthy. The function result of the function returned by `elseif`, `if`, and the function returned by `else` all return a function which returns the return value of the executed function body, allowing you to write your conditional blocks in a way similar to Ruby.

## example

```js
import when from 'operator/if';

console.log(when(false)(() => 1).elseif(false)(() => 2).else(() => 3)());

// 3
```

'use strict';

import assign from 'object-assign';

const noop = () => {};

const makeCall = (exec) => assign(() => exec(), {
  elseif: (next) => (body) => (exec === noop && next && makeCall(body) || makeCall(exec)),
  else: (body) => (exec === noop && (() => body()) || (() => exec()))
});

export default (cond) => (body) => cond && makeCall(body) || makeCall(noop);

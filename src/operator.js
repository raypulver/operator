'use strict';

import when from './if';
import attempt from './try';

export {
  when,
  attempt
};

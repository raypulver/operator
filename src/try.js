'use strict';

import assign from 'object-assign';

export default (body) => {
  return {
    catch: (fn) => {
      const call = () => {
        try {
          return body();
        } catch (e) {
          return fn(e);
        }
      };
      return assign(call, {
        finally: (fn) => () => (call(), fn())
      });
    },
    finally: (fn) => () => {
      try {
        body();
      } finally {
        fn();
      }
    }
  };
};
